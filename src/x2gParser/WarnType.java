import java.util.Set;

public class WarnType {
	private Set<String> level;
	public static final String
		NONE = "none",
		GENERAL = "general",
		EMPTYSET = "emptyset",
		MISSINGNODES = "missingnodes",
		MISSINGKEYVALUE = "missingkeyvalue",
		ALL = "all";
	public WarnType() { set(NONE); }
	void set(String val) { level.add(val); }
	Set<String> get() { return level; }
	boolean is(String val) { return level.contains(val); }
}

// vim: ff=unix ts=3 sw=3 sts=3 noet
