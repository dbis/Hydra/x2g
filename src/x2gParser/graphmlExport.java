import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.io.*;
import org.jgrapht.nio.*;
import org.jgrapht.nio.graphml.GraphMLExporter;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * Export the graph as GraphML file.
 *
 * @param graph the gGraph
 * @param baseIRI (optional) base IRI
 * @param pm (optional) prefix mapping
 * @param outputFile the output file
 */
public class graphmlExport {
	public static void asGraph(gGraph ggraph, File outputFile) {
		Objects.requireNonNull(ggraph);
		Objects.requireNonNull(outputFile);

		final Graph<gNode, gEdge> graph = new DefaultDirectedGraph<>(gEdge.class);
		buildGraph(graph, ggraph);
		ComponentNameProvider<gNode> vertexIDProvider = vertex -> String.valueOf(vertex.getId());
		ComponentNameProvider<gNode> vertexNameProvider = gNode::getLabel;
		ComponentNameProvider<gEdge> edgeIDProvider = edge -> String.valueOf(edge.getId());
		ComponentNameProvider<gEdge> edgeLabelProvider = gEdge::getLabel;
		GraphMLExporter<gNode, gEdge> exporter = new GraphMLExporter<>(
		    vertexIDProvider, vertexNameProvider,
		    edgeIDProvider, edgeLabelProvider);
		try {
			exporter.exportGraph(graph, new FileWriter(outputFile));
		} catch (IOException | ExportException e) {
			log.error("failed to write graph to file " + outputFile, e);
		}
	}

	public static Graph<gNode, gEdge> buildGraph(Graph<gNode, gEdge> graph, gGraph ggraph) {
		return graph;
	}
}

// vim: spell spelllang=en
// vim: ff=unix ts=3 sw=3 sts=3 noet
