# JAVA
SHELL = /bin/bash
JAVAC = javac
JFLAGS = -g -cp $(CLASSPATH) -d $(CLASSDIR) -Xlint:deprecation -Xlint:unchecked
#CLASSDIR = $(TARGET)
CLASSDIR = .
CLASSPATH=".:$(COMMONS):$(JACKSON):$(JDOM):$(JAXEN):$(AJARS):$(JGRAPHT):$(CLASSDIR):"
# ANTLR4 expecting version 4.9.3
ANTLR = antlr4
#AFLAGS = -o $(TARGET) -package $(TARGET)
AFLAGS = -visitor -no-listener
ASRCS = $(wildcard *.g4)
LIBDIR = /usr/share/java/antlr4
AJARS = "$(LIBDIR)/antlr4.jar:$(LIBDIR)/antlr4-runtime.jar"
COMMONS = /usr/share/java/apache-commons-cli.jar:java/commons-csv-1.12.0.jar
JACKSON = /usr/share/java/jackson-core.jar:/usr/share/java/jackson-databind.jar:/usr/share/java/jackson-annotations.jar
#JDOM = /usr/share/java/jdom2/jdom2.jar
JDOM = ./jdom2/jdom-2.0.6.1.jar
#JAXEN = /usr/share/java/jaxen.jar
JAXEN = ./jdom2/lib/jaxen-1.2.0.jar
# jgrapht
JGRAPHT = jgrapht-1.5.2/lib/jgrapht-core-1.5.2.jar:jgrapht-1.5.2/lib/jgrapht-io-1.5.2.jar
# X2G
TARGET = x2g
SOURCES = $(MYSRCS) $(GENSRCS)
GRAPHSRCS = \
	gElement.java \
	gEdge.java \
	gGraph.java \
	gNode.java \
	gProperties.java \
	gTest.java \
	csvExport.java
#	graphmlExport.java 
XMLSRCS = \
	xTractor.java
MYSRCS = \
	Evaluator.java \
	Scope.java \
	SymbolTable.java \
	Variable.java \
	VarType.java \
	WarnType.java \
	$(GRAPHSRCS) \
	$(XMLSRCS)
#LISTENER = x2gParserBaseListener.java x2gParserListener.java
VISITOR = x2gParserBaseVisitor.java x2gParserVisitor.java
GENSRCS = $(LISTENER) $(VISITOR) $(ASRCS:.g4=.java)
GENCLASSES = $(GENSRCS:.java=.class)
MYCLASSES = $(MYSRCS:.java=.class)
CLASSES = $(GENCLASSES) $(MYCLASSES)
MANIFEST = Manifest.txt
TOKENS = x2gParser.tokens x2gLexer.tokens

all::	Main.class

all::	$(CLASSES)

#$(CLASSES)::	$(SOURCES)

Main.class:	Main.java
Main.java:	$(SOURCES)

x2gParserListener.java:	x2gParser.java
x2gParserVisitor.java:	x2gParser.java
x2gParserBaseListener.java:	x2gParserListener.java
x2gParserBaseVisitor.java:	x2gParserVisitor.java
Evaluator.java:	x2gParserBaseVisitor.java
x2gParser.java:	x2gLexer.java

run:	runhelp runparse werewolf graphtest

runhelp:
	export CLASSPATH=$(CLASSPATH) ; java Main -h

runparse:
	export CLASSPATH=$(CLASSPATH) ; java Main -vp <testunit/simple-ruleset-with-errors.x2g
	export CLASSPATH=$(CLASSPATH) ; java Main -vp <testunit/simple-ruleset.x2g

werewolf:
	export CLASSPATH=$(CLASSPATH) ; java Main -wn -o werewolf -d isebel/werewolf <testunit/isebel-stories.x2g

isebel-werewolf:
	export CLASSPATH=$(CLASSPATH) ; java Main -wn -o isebel-werewolf -d isebel/werewolf <testunit/isebel-story-nodes.x2g

wossidia-werewolf:
	export CLASSPATH=$(CLASSPATH) ; java Main -wn -o wossidia-werewolf -d isebel/wossidia-werewolf <testunit/isebel-story-nodes.x2g
	#export CLASSPATH=$(CLASSPATH) ; java Main -wn --csv-style assign -o wossidia-werewolf -d isebel/wossidia-werewolf <testunit/isebel-story-nodes.x2g

wossidia-werewolf-old:
	export CLASSPATH=$(CLASSPATH) ; java Main -wn -o wossidia-werewolf-old -d isebel/wossidia-werewolf <testunit/isebel-stories.x2g

wossidia-werewolf-edges:
	export CLASSPATH=$(CLASSPATH) ; java Main -wn -o wossidia-werewolf-e -d isebel/wossidia-werewolf <testunit/isebel-story-edges.x2g

wossidia-werewolf-hedges:
	export CLASSPATH=$(CLASSPATH) ; java Main -wn -o wossidia-werewolf-h -d isebel/wossidia-werewolf <testunit/isebel-story-hedges.x2g

isebel-witches:
	export CLASSPATH=$(CLASSPATH) ; java Main --csv-style json -wv -o isebel-witches -d isebel/witches <testunit/isebel-story-nodes.x2g

isebel-witches-dk:
	export CLASSPATH=$(CLASSPATH) ; java Main -w -o isebel-witches-dk -d isebel/witches-dk <testunit/isebel-story-nodes.x2g

isebel-witches-nl:
	export CLASSPATH=$(CLASSPATH) ; java Main -w -o isebel-witches-nl -d isebel/witches-nl <testunit/isebel-story-nodes.x2g

isebel-all:	isebel-iceland-knowledge isebel-denmark isebel-netherlands isebel-mecklenburg isebel-iceland-knowledge isebel-iceland-co-occurence

isebel-iceland-knowledge:
	export CLASSPATH=$(CLASSPATH) ; java Main -w -o isebel-iceland -f csv --csv-style json -d isebel/isebel_icelandic <testunit/isebel-story-nodes.x2g

isebel-iceland-co-occurence:
	export CLASSPATH=$(CLASSPATH) ; java Main -w -o isebel-iceland-co-occurence -f csv --csv-style json -d isebel/isebel_icelandic <testunit/isebel-story-edges.x2g

isebel-denmark:
	#export CLASSPATH=$(CLASSPATH) ; java Main -w --warn-empty -o isebel-denmark -d isebel/isebel_ucla <testunit/isebel-story-nodes.x2g
	export CLASSPATH=$(CLASSPATH) ; java Main -w --csv-style json -o isebel-denmark -d isebel/isebel_ucla <testunit/isebel-story-nodes.x2g

isebel-netherlands:
	export CLASSPATH=$(CLASSPATH) ; java Main -w -o isebel-netherlands -f csv --csv-style json -d isebel/isebel_verhalenbank <testunit/isebel-story-nodes.x2g

isebel-mecklenburg:
	export CLASSPATH=$(CLASSPATH) ; java Main -w -o isebel-mecklenburg -f csv --csv-style json -d isebel/isebel_rostock <testunit/isebel-story-nodes.x2g

isebel-witches-edges:
	export CLASSPATH=$(CLASSPATH) ; java Main -wt -o isebel-witches -d isebel/witches <testunit/isebel-story-edges.x2g

werewolf1:
	export CLASSPATH=$(CLASSPATH) ; java Main -wn -o werewolf001 -d isebel/werewolf001 <testunit/isebel-stories.x2g

graphtest:
	export CLASSPATH=$(CLASSPATH) ; for g in testunit/*.x2g ; do java -ea gTest <$$g ; done

xpathtest:
	export CLASSPATH=$(CLASSPATH) ; java -ea xTractor testunit/catalog.xml "//cd"

cdcatalog:
	export CLASSPATH=$(CLASSPATH) ; java Main -s -f csv --csv-style json -o catalog testunit/catalog.xml <testunit/catalog.x2g

grun:
	export CLASSPATH=$(CLASSPATH) ; for g in testunit/*.x2g ; do java org.antlr.v4.gui.TestRig x2g x2g -token -tree -gui <$$g ; done

clean:
	-rm -rf $(GENSRCS) $(TOKENS) $(CLASSES) *.class

.SUFFIXES:

.SUFFIXES:	.g4 .tokens .interp .java .class .jar

.java.class:
	$(JAVAC) $(JFLAGS) $<

.g4.interp:
	$(ANTLR) $(AFLAGS) $<

.g4.tokens:
	$(ANTLR) $(AFLAGS) $<

.g4.java:
	$(ANTLR) $(AFLAGS) $<

jar:    
	@echo "Manifest-Version: 1.0" > $(MANIFEST)
	@echo "Class-Path: ." >> $(MANIFEST)
	@echo "Main-Class: $(TARGET)" >> $(MANIFEST)
	@echo "" >> $(MANIFEST)
	jar -cmf $(MANIFEST) $(TARGET).jar $(CLASSES)

# vim: ts=3 sw=3 sts=3 noet
