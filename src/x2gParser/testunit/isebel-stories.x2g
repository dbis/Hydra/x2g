// nested match example, the second match is evaluated within the context of the first
match xpath("//isebel:story") using $s {
	create node $sn label "story" {
		// properties
		// or for each content in different laguage a separate node?
		content = $s.xpath("isebel:contents/isebel:content/text()"),
		title = $s.xpath("dc:title/text()")
		id = $s.xpath("dc:identifier/text()")
		unique (id)
	},

	match xpath(".//isebel:person[isebel:role/text()='narrator']") using $p {
		create node $pn label "person" {
			// properties with optional type declaration
			name = $p.xpath("isebel:name/text()"),
			age = $p.xpath("isebel:age"),
			// conditional assignment
			if ($p.xpath("isebel:gender/text()") == "female") {
				gender = "female-narrator"
			}
			else {
				gender = "male-narrator"
			},
			unique (name, gender)
		},
		create edge $e from $sn to $pn label "narrator" {
			alt = "story-teller"
			// no more properties given
		}
	},

	match xpath(".//isebel:keyword") using $k {
		create node $kn label "keyword" {
			value = $k.xpath("text()"),
			prov = $k.xpath("@provenance")
		},
		create edge $e from $sn to $kn label "content" {
			alt = "is-about"
		}
	}
}

// vim: spell spelllang=en
// vim: ff=unix ts=3 sw=3 sts=3 noet
