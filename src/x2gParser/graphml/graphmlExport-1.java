import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.AttributeType;
import org.jgrapht.nio.DefaultAttribute;
import org.jgrapht.nio.graphml.GraphMLExporter;

public class ExportController {
	public String exportGraph(String url) {
		try {
			// Create a graph
			Graph<NodeDto, EdgeDto> graph = new DefaultDirectedGraph<>(EdgeDto.class);
			JsonNode nodes = jsonNode.get("nodes");
			ArrayList<NodeDto>  fullnode = new ArrayList<NodeDto>();
			for (Iterator<Map.Entry<String, JsonNode>> it = nodes.fields(); it.hasNext(); ) {
				Map.Entry<String, JsonNode> entry = it.next();
				NodeDto  nodee = new NodeDto();
				String nodeId = entry.getKey();
				String signature = entry.getValue().get("signature").asText();
				String type = entry.getValue().get("type").toString();
				nodee.setId(nodeId);
				nodee.setLabel(nodeId);
				nodee.setSignature(signature);
				nodee.setType(type);
				fullnode.add(nodee);
				graph.addVertex(nodee);
			}
			// Extract and add edges to the graph
			JsonNode edges = jsonNode.get("edges");
			for (Iterator<Map.Entry<String, JsonNode>> it = edges.fields(); it.hasNext(); ) {
				Map.Entry<String, JsonNode> entry = it.next();
				String edgeid = entry.getKey();
				String type = entry.getValue().get("type").toString();
				String sourceNodeId = entry.getValue().get("links").get("0").get("node").asText();
				String targetNodeId = entry.getValue().get("links").get("1").get("node").asText();
				NodeDto  srcnode = new NodeDto();
				NodeDto  targetnode = new NodeDto();
				for (int i=0; i <fullnode.size(); i++) {
					NodeDto ee = fullnode.get(i);
					if (ee.getId().equals(sourceNodeId)) {
						srcnode = ee;
					}
					if (ee.getId().equals(targetNodeId)) {
						targetnode =ee;
					}
				}
				// Add the edge to the graph
				EdgeDto ee = graph.addEdge(srcnode, targetnode);
				ee.setId(edgeid);
				ee.setTarget(targetNodeId);
				ee.setSource(sourceNodeId);
				ee.setType(type);
			}
			// Complete the GraphML content
			//  graphmlContent.append("</graphml>");
			GraphMLExporter<NodeDto, EdgeDto> exporter = new GraphMLExporter<>();
			exporter.setVertexIdProvider(new Function<NodeDto, String>() {
				@Override
				public String apply(NodeDto nodeDto) {
					return nodeDto.getLabel();
				}
				@Override
				public <V> Function<V, String> compose(Function<? super V, ? extends NodeDto> before) {
					return Function.super.compose(before);
				}
				@Override
				public <V> Function<NodeDto, V> andThen(Function<? super String, ? extends V> after) {
					return Function.super.andThen(after);
				}
			});
			exporter.setVertexAttributeProvider(new Function<NodeDto, Map<String, Attribute>>() {
				@Override
				public Map<String, Attribute> apply(NodeDto graphMember) {
					Map<String, Attribute> toReturn = new HashMap<>();
					toReturn.put("label", DefaultAttribute.createAttribute(graphMember.getLabel()));
					toReturn.put("signature", DefaultAttribute.createAttribute(graphMember.getSignature()));
					toReturn.put("type", DefaultAttribute.createAttribute(graphMember.getType()));
					return toReturn;
				}
				@Override
				public <V> Function<V, Map<String, Attribute>> compose(Function<? super V, ? extends NodeDto> before) {
					return Function.super.compose(before);
				}
				@Override
				public <V> Function<NodeDto, V> andThen(Function<? super Map<String, Attribute>, ? extends V> after) {
					return Function.super.andThen(after);
				}
			});
			exporter.setEdgeAttributeProvider(new Function<EdgeDto, Map<String, Attribute>>() {
				@Override
				public Map<String, Attribute> apply(EdgeDto edgeDto) {
					Map<String, Attribute> toReturn = new HashMap<>();
					toReturn.put("id", DefaultAttribute.createAttribute(edgeDto.getId()));
					toReturn.put("sourceid", DefaultAttribute.createAttribute(edgeDto.getSource()));
					toReturn.put("targetid", DefaultAttribute.createAttribute(edgeDto.getTarget()));
					toReturn.put("type", DefaultAttribute.createAttribute(edgeDto.getType()));
					return toReturn;
				}
				@Override
				public <V> Function<V, Map<String, Attribute>> compose(Function<? super V, ? extends EdgeDto> before) {
					return Function.super.compose(before);
				}
			});
			exporter.registerAttribute("label", GraphMLExporter.AttributeCategory.NODE, AttributeType.STRING);
			exporter.registerAttribute("signature", GraphMLExporter.AttributeCategory.NODE, AttributeType.STRING);
			exporter.registerAttribute("type", GraphMLExporter.AttributeCategory.NODE, AttributeType.STRING);
			exporter.registerAttribute("id", GraphMLExporter.AttributeCategory.EDGE, AttributeType.STRING);
			exporter.registerAttribute("sourceid", GraphMLExporter.AttributeCategory.EDGE, AttributeType.STRING);
			exporter.registerAttribute("targetid", GraphMLExporter.AttributeCategory.EDGE, AttributeType.STRING);
			exporter.registerAttribute("type", GraphMLExporter.AttributeCategory.EDGE, AttributeType.STRING);
			exporter.setExportEdgeLabels(true);
			try (OutputStream outputStream = new FileOutputStream("graph.graphml")) {
				exporter.exportGraph(graph, outputStream);
				Path fileName = Path.of("graph.graphml");
				// Now calling Files.readString() method to
				// read the file
				String str = Files.readString(fileName);
				res.setGraphml(str);
				System.out.println(res.getGraphml());
			} catch (IOException e) {
				e.printStackTrace();
				// Handle the file writing error, and send an error response to the client
				System.err.println("Error writing GraphML file: " + e.getMessage());
			}
			res.setGraph(graphData.toString());
			return res;
			// return graphData.toString();
		} catch (HttpClientErrorException ex) {
			System.out.println("Error response status code: " + ex.getRawStatusCode());
			System.out.println("Error response body: " + ex.getResponseBodyAsString());
		} catch (JsonMappingException e) {
			throw new RuntimeException(e);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
		return res;
	}
}

// vim: spell spelllang=en
// vim: ff=unix ts=3 sw=3 sts=3 noet
